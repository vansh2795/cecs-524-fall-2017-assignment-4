﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace CECS_524_Assignment
{
    class Program
    {

        static IEnumerable<string> EnumerateFilesRecursively(string path)
        {
            foreach (var file in Directory.GetFiles(path))
            {
                yield return file;
            }

            foreach (var directory in Directory.GetDirectories(path))
            {
                foreach (var file in EnumerateFilesRecursively(directory))
                    yield return file;
            }
        }

        static string FormatByteSize(long byteSize)
        {
            string[] sizes = { "B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB" };
            decimal dsize = Convert.ToDecimal(byteSize);
            int i = 0;


            while (dsize >= 1000 && i < sizes.Length - 1)
            {
                i++;
                dsize = dsize / 1000m;
            }

            return String.Format("{0:0.00} {1}", dsize, sizes[i]);
        }

        static XDocument CreateReport(IEnumerable<string> files)
        {
            var extensionInf = from file in files
                               group file by Path.GetExtension(file) into g
                               let size = g.Sum(y => new System.IO.FileInfo(y).Length)
                               let count = g.Count()

                               select new { Extension = g.Key, Count = count, Size = size };

            XDocument doc = new XDocument(
                new XElement("Extensions",
                 from ext in extensionInf.AsEnumerable()
                 orderby ext.Extension ascending
                 select new XElement("Extension",
                      new XAttribute("Type", ext.Extension),
                      new XAttribute("Count", ext.Count),
                      new XAttribute("Size", FormatByteSize(ext.Size)))
                 ));

            XDocument htmltable = new XDocument(
            new XElement("table", new XAttribute("border", 1),
                    new XElement("thead",
                        new XElement("tr",
                            new XElement("th", "Type"),
                            new XElement("th", "Count"),
                            new XElement("th", "Size"))),
                    new XElement("tbody",
                    from d in doc.Descendants("Extension")
                    select new XElement("tr",
                                new XElement("td", d.Attribute("Type").Value),
                                new XElement("td", d.Attribute("Count").Value),
                                new XElement("td", d.Attribute("Size").Value)))));

            return htmltable;
        }
        static int Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Enter two Arguments");
                Console.ReadLine();
                return 1;
            }

            try
            {
                CreateReport(EnumerateFilesRecursively(args[0])).Save(args[1]);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : " + ex.Message);
                Console.ReadLine();
            }

            return 0;
        }

    }
}